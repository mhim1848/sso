import React, { useState } from 'react';

function App() {
  const [hovering, setHovering] = useState(false);
  return (
    <div
      className={`app ${hovering ? 'app__hover' : ''}`}
      onMouseOver={() => setHovering(!hovering)}
    >
      <p>App</p>
    </div>
  );
}

export default App;
